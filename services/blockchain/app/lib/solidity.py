# Copyright 2023, Atos Spain S.A.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from solcx import compile_source, install_solc
from web3 import Web3
from web3.exceptions import TimeExhausted
from web3.middleware import geth_poa_middleware

install_solc("0.8.0")


def parse_params(params: dict) -> list:
    return [_ for _ in params.values() if _ is not None]


def parse_md(contract, ignore_functions=["verify"]) -> list:
    return [
        contract.functions[f]().call()
        for f in contract.functions
        if f not in ignore_functions
    ]


class DatasetContractParams:
    def __init__(self, data_params: dict):
        self._params = parse_params(data_params)

    def get(self) -> list:
        return self._params


class ModelContractParams:
    def __init__(self, model_params: dict, data_params: dict):
        self._model_params = parse_params(model_params)
        self._data_params = parse_params(data_params)

    def get(self) -> list:
        return self._model_params + self._data_params


class ModelContractParamsUnverified:
    def __init__(self, model_params: dict):
        self._params = parse_params(model_params)

    def get(self) -> list:
        return self._params


class Contract:
    def __init__(self, path):
        self._compiled = compile_source(
            open(path, "r").read(), output_values=["abi", "bin"]
        )
        self.id, self.interface = self._compiled.popitem()

    def read(self, client, address: str):
        self._contract = client.eth.contract(
            address=address, abi=self.interface["abi"]
        )

    def get_functions(self):
        if self._remote_vars:
            return self._remote_vars.all_functions()

    def get_all_vars(self):
        if self._remote_vars:
            return [i["name"] for i in self._remote_vars.abi if "name" in i]


class W3Client:
    def __init__(self, endpoint: str, address: str, private_key: str):
        self._client = Web3(Web3.HTTPProvider(endpoint))
        # Middleware for PoA
        self._client.middleware_onion.inject(geth_poa_middleware, layer=0)
        self._address = W3Client.checksum_address(address)
        self._private_key = private_key

    def client(self):
        return self._client

    @staticmethod
    def checksum_address(address: str) -> str:
        try:
            return Web3.toChecksumAddress(address)
        except Exception as e:
            print(f"Invalid address {address}:\n\n{e}")

    def get_nonce(self):
        return hex(
            self._client.eth.getTransactionCount(self._address, "pending")
        )

    def read_contract(self, contract: Contract, address: str):
        address = W3Client.checksum_address(address)
        contract = self._client.eth.contract(
            abi=contract.interface["abi"], address=address
        )
        return parse_md(contract)

    def deploy_contract(
        self,
        contract: Contract,
        tx_params: list,
        increase_factor: float = 1.1,
    ) -> str:
        contract = self._client.eth.contract(
            abi=contract.interface["abi"], bytecode=contract.interface["bin"]
        )
        constructor_txn = contract.constructor(*tx_params)
        estimated_gas = constructor_txn.estimateGas({"from": self._address})
        suggested_gas_price = self._client.eth.gasPrice
        current_gas_price = suggested_gas_price
        while True:
            txn = constructor_txn.buildTransaction(
                {
                    "from": self._address,
                    "nonce": self.get_nonce(),
                    "gas": estimated_gas,
                    "gasPrice": current_gas_price,
                }
            )
            signed_txn = self._client.eth.account.signTransaction(
                txn, self._private_key
            )
            try:
                self._client.eth.sendRawTransaction(signed_txn.rawTransaction)
                tx_hash = self._client.toHex(
                    self._client.keccak(signed_txn.rawTransaction)
                )
                tx_rec = self._client.eth.wait_for_transaction_receipt(
                    tx_hash, timeout=120
                )
                return tx_rec.contractAddress
            except TimeExhausted:
                print(
                    "Transaction was not mined within 120 seconds. Increasing gas price and trying again."
                )
                current_gas_price = int(current_gas_price * increase_factor)
            except ValueError as e:
                if "replacement transaction underpriced" in str(e):
                    print(
                        "Replacement transaction underpriced. Increasing gas price and trying again."
                    )
                    current_gas_price = int(
                        current_gas_price * increase_factor
                    )
                else:
                    raise

    def call_contract_function(
        self,
        address: str,
        contract: Contract,
        function: str,
        tx_params: list,
        increase_factor: float = 1.1,
    ):
        address = W3Client.checksum_address(address)
        contract = self._client.eth.contract(
            abi=contract.interface["abi"], address=address
        )
        constructor_txn = contract.functions[function](*tx_params)
        estimated_gas = constructor_txn.estimateGas({"from": self._address})
        suggested_gas_price = self._client.eth.gasPrice
        current_gas_price = suggested_gas_price
        while True:
            txn = constructor_txn.buildTransaction(
                {
                    "from": self._address,
                    "nonce": self.get_nonce(),
                    "gas": estimated_gas,
                    "gasPrice": current_gas_price,
                }
            )
            signed_txn = self._client.eth.account.signTransaction(
                txn, self._private_key
            )
            try:
                self._client.eth.sendRawTransaction(signed_txn.rawTransaction)
                tx_hash = self._client.toHex(
                    self._client.keccak(signed_txn.rawTransaction)
                )
                tx_rec = self._client.eth.wait_for_transaction_receipt(
                    tx_hash, timeout=120
                )
                return tx_rec
            except TimeExhausted:
                print(
                    "Transaction was not mined within 120 seconds. Increasing gas price and trying again."
                )
                current_gas_price = int(current_gas_price * increase_factor)
            except ValueError as e:
                if "replacement transaction underpriced" in str(e):
                    print(
                        "Replacement transaction underpriced. Increasing gas price and trying again."
                    )
                    current_gas_price = int(
                        current_gas_price * increase_factor
                    )
                else:
                    raise
