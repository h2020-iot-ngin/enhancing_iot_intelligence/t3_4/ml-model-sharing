// Copyright 2023, Atos Spain S.A.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived from
//       this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

pragma solidity >=0.7.0;
pragma experimental ABIEncoderV2;

contract Model {
    struct ModelParams {
        string developer_id;
        string organization_id;
        string train_image;
        string res_hash;
    }

    struct DataParams {
        string dataset_id;
    }

    ModelParams _model_params;
    DataParams _data_params;

    constructor(
        string memory developer_id,
        string memory organization_id,
        string memory train_image,
        string memory dataset_id
    ) public {
        _model_params.developer_id = developer_id;
        _model_params.organization_id = organization_id;
        _model_params.train_image = train_image;
        _model_params.res_hash = "";
        _data_params.dataset_id = dataset_id;
    }

    function verify(string memory res_hash) public {
        if (bytes(_model_params.res_hash).length == 0) {
            _model_params.res_hash = res_hash;
        }
    }

    function get_model_params()
        public
        view
        returns (
            string memory,
            string memory,
            string memory,
            string memory
        )
    {
        return (
            _model_params.developer_id,
            _model_params.organization_id,
            _model_params.train_image,
            bytes(_model_params.res_hash).length == 0
                ? "NOT VERIFIED"
                : _model_params.res_hash
        );
    }

    function get_data_params() public view returns (string memory) {
        return (_data_params.dataset_id);
    }
}
