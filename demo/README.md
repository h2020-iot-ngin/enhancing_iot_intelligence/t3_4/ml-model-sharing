# Machine Learning Models Sharing Platform - Use Case Demo
## Local Cluster Deployment
### Prerequisites
- [Docker Engine](https://docs.docker.com/get-docker/)
- [MiniKube](https://minikube.sigs.k8s.io/docs/start/)

```bash
# Start Minikube
minikube start --driver=docker --cpus=4 --memory=4g

# Enable ingress
minikube addons enable ingress

# Add minikube ip to /etc/hosts
echo "$(minikube ip) ml-models-sharing-platform.local" | sudo tee -a /etc/hosts

minikube kubectl -- apply -k ./k8s/local
```