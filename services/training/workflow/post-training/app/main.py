# Copyright 2023, Atos Spain S.A.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os
import subprocess
import sys

from mlaas_sdk.clients import SharingClient
from mlaas_sdk.exceptions import sharing as SharingExceptions

from .core.config import (
    INPUT_DIR,
    MODEL_ID,
    SHARING_ENDPOINT,
    TMP_DIR,
)

sharing_svc = SharingClient(SHARING_ENDPOINT)

if __name__ == "__main__":
    try:
        match len(os.listdir(INPUT_DIR)):
            case 0:
                print("No models were found in the input directory")
                sys.exit(1)
            case 1:
                model_path = os.path.join(INPUT_DIR, os.listdir(INPUT_DIR)[0])
            case _:
                model_path = os.path.join(TMP_DIR, f"{MODEL_ID}.tar.gz")
                subprocess.call(
                    ["tar", "-C", INPUT_DIR, "-zcvf", model_path, "."]
                )
        sharing_svc.update_model(MODEL_ID, model_path)
        sys.exit(0)
    except SharingExceptions.SharingException as e:
        print(f"Error uploading model: {e}")
        sys.exit(1)
