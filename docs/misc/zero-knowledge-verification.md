# Zero Knowledge Verification
The Zero Knowledge Verification property is ensured by the platform for all the datasets and models registered.

Each model to be registered in the platform must specify:
- the dataset to be used for training (must be already registered in the platform)
- a container image with the model training instructions

By storing the following information in the Blockchain, and overseeing the training phase of the model in a controlled environment, we can guarantee the replicability of all the trained models, and ensure that the training resuls are achieved exclusively with the specified inputs.

All aforementioned metadata is stored in the blockchain. This ensures its inmutability, and allows the platform to enforce the integrity of all registered artifacts.
