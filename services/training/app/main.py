# Copyright 2023, Atos Spain S.A.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from typing import Optional
import yaml

from .core.config import (
    ARGO_ENDPOINT,
    SHARING_ENDPOINT,
    ARGO_NAMESPACE,
    WORKFLOW_TEMPLATE,
)

from hera.workflows import Workflow, WorkflowsService
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware


app = FastAPI(
    middleware=[
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_methods=["*"],
            allow_headers=["*"],
        )
    ]
)


class PayloadModelTraining(BaseModel):
    model_id: str
    dataset_id: str
    training_image: str
    sharing_endpoint: Optional[str] = SHARING_ENDPOINT


@app.get("/ping")
async def ping():
    """Liveness probe endpoint."""
    return "pong"


@app.post("/train/{model_id}")
async def train_model(payload: PayloadModelTraining):
    """Schedule a model train job in the platform."""
    try:
        with open(WORKFLOW_TEMPLATE) as file:
            workflow_dict = yaml.safe_load(file)
        workflow_dict["spec"]["arguments"]["parameters"] = [
            {"name": ar["name"], "value": payload.dict()[ar["name"]]}
            for ar in workflow_dict["spec"]["arguments"]["parameters"]
        ]
        with Workflow.from_dict(workflow_dict) as wf:
            wf.workflows_service = WorkflowsService(
                host=ARGO_ENDPOINT, verify_ssl=False
            )
            wf.namespace = ARGO_NAMESPACE
            wf.create(wait=True)
            return {"Model training scheduled."}
    except Exception as e:
        print(f"Error scheduling training workflow: {e}")
        return {"Error scheduling model training."}
