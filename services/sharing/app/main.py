# Copyright 2023, Atos Spain S.A.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import io
import os
from typing import Union
from uuid import uuid4

from .core.config import (
    BLOCKCHAIN_ENDPOINT,
    DOWNLOAD_DIR,
    MINIO_ACCESS,
    MINIO_DATASETS_BUCKET,
    MINIO_ENDPOINT,
    MINIO_MODELS_RES_BUCKET,
    MINIO_SECRET,
    MINIO_SECURE,
    TRAINING_ENDPOINT,
)
from .core.db import db
from .lib.hashing import get_hash
from .lib.minio_client import MinioClient
from .models import Dataset, MLModel

from fastapi import FastAPI, File, Form, UploadFile
from mlaas_sdk.clients import BlockchainClient, TrainingClient
from mlaas_sdk.models import (
    DatasetContract,
    ModelContract,
    ModelContractUnverified,
)
import shutil
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import FileResponse

minio = MinioClient(
    MINIO_ENDPOINT,
    MINIO_SECURE,
    MINIO_ACCESS,
    MINIO_SECRET,
    MINIO_DATASETS_BUCKET,
    MINIO_MODELS_RES_BUCKET,
)

app = FastAPI(
    app=FastAPI(max_upload_size=2048 * 1024 * 1024),
    middleware=[
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_methods=["*"],
            allow_headers=["*"],
        )
    ],
)

blockchain_svc = BlockchainClient(BLOCKCHAIN_ENDPOINT)
training_svc = TrainingClient(TRAINING_ENDPOINT)


def get_model_metadata(
    model_id,
) -> Union[DatasetContract, ModelContract, ModelContractUnverified]:
    """Get model metadata from blockchain."""
    model = MLModel.get_by_id(model_id)
    return blockchain_svc.read_contract("model", model.contract_address)


@app.on_event("startup")
async def startup_event():
    db.connect()
    db.create_tables([MLModel, Dataset], safe=True)


@app.on_event("shutdown")
async def shutdown_event():
    db.close()


@app.get("/ping/")
async def ping():
    """Liveness probe endpoint."""
    return "pong"


@app.get("/model/{model_id}/")
async def get_model(model_id: str):
    """Fetch a stored model."""
    try:
        model = MLModel.get_by_id(model_id)
        if model.unverified or model.trained:
            obj = minio.client.get_object(
                minio.models_res_bucket, model_id
            ).read()
            down_dir = os.path.join(DOWNLOAD_DIR, model_id)
            if not os.path.exists(down_dir):
                os.makedirs(down_dir)
            else:
                shutil.rmtree(down_dir)
                os.makedirs(down_dir)
            file = os.path.join(down_dir, model.res_filename)
            with open(file, "wb") as f:
                f.write(obj)
            return FileResponse(
                file,
                media_type=model.res_content_type,
                filename=model.res_filename,
            )
        else:
            return f"Model {model_id} has not been trained yet. Please, try again later."
    except Exception as e:
        print(e)
        return f"Model {model_id} not found."


@app.get("/dataset/{dataset_id}/")
async def get_dataset(dataset_id: str):
    """Fetch a stored dataset."""
    try:
        dataset = Dataset.get_by_id(dataset_id)
        obj = minio.client.get_object(minio.datasets_bucket, dataset_id).read()
        down_dir = os.path.join(DOWNLOAD_DIR, dataset_id)
        if not os.path.exists(down_dir):
            os.makedirs(down_dir)
        else:
            shutil.rmtree(down_dir)
            os.makedirs(down_dir)
        file = os.path.join(down_dir, dataset.filename)
        with open(file, "wb") as f:
            f.write(obj)
        return FileResponse(
            file, media_type=dataset.content_type, filename=dataset.filename
        )
    except Exception as e:
        print(e)
        return f"Dataset {dataset_id} not found."


@app.post("/model/")
async def add_model(
    metadata: ModelContract,
):
    """Register, store and train a new model in the platform."""
    try:
        uid = str(uuid4())
        address = blockchain_svc.deploy_contract("model", metadata)
        db_mod = MLModel.create(
            id=uid,
            train_image=metadata.model_params.train_image,
            contract_address=address,
            organization_id=metadata.model_params.organization_id,
        )
        training_svc.train_model(address)
        return {"model_id": db_mod.id}
    except Exception as e:
        print(e)
        return {"Error storing model."}


@app.post("/dataset/")
async def add_dataset(
    dataset: UploadFile = File(...),
    metadata: str = Form(media_type="application/json"),
):
    """Register and store a new dataset in the platform."""
    try:
        dataset_bytes = await dataset.read()
        dataset_buffer = io.BytesIO(dataset_bytes)
        metadata = DatasetContract.parse_raw(metadata)
        metadata.size_bytes = len(dataset_bytes)
        metadata.hash = get_hash(dataset_buffer)
        uid = str(uuid4())
        dataset_buffer.seek(0)
        address = blockchain_svc.deploy_contract("dataset", metadata)
        minio.client.put_object(
            MINIO_DATASETS_BUCKET,
            uid,
            dataset_buffer,
            metadata.size_bytes,
            metadata={
                "Content-Type": dataset.content_type,
                "Metadata": metadata.dict(exclude_none=True),
            },
        )
        db_mod = Dataset.create(
            id=uid,
            filename=dataset.filename,
            content_type=dataset.content_type,
            contract_address=address,
            organization_id=metadata.organization_id,
        )
        return {"dataset_id": db_mod.id}
    except Exception as e:
        print(e)
        return {"Error storing dataset."}


@app.post("/model/unverified/")
async def add_model(
    model: UploadFile = File(...),
    metadata: str = Form(media_type="application/json"),
):
    """Registers and stores a new model in the platform.
    This endpoint does not leverage neither the ZKV nor training services, and is only intended for testing purposes.
    """
    try:
        model_bytes = model.file.read()
        model_buffer = io.BytesIO(model_bytes)
        metadata = ModelContractUnverified.parse_raw(metadata)
        metadata.hash = get_hash(model_buffer)
        uid = str(uuid4())
        model_buffer.seek(0)
        minio.client.put_object(
            MINIO_MODELS_RES_BUCKET,
            uid,
            model_buffer,
            len(model_bytes),
            metadata={
                "Content-Type": model.content_type,
                "Metadata": metadata.dict(exclude_none=True),
            },
        )
        db_mod = MLModel.create(
            id=uid,
            res_filename=model.filename,
            res_content_type=model.content_type,
            organization_id=metadata.organization_id,
            unverified=True,
        )
        return {"model_id": db_mod.id}
    except Exception as e:
        print(e)
        return {"Error storing model."}


@app.put("/model/{model_id}/")
async def add_trained_model(
    model_id: str,
    model: UploadFile = File(...),
):
    """Stores the result of a model training job in the platform, and verifies the model."""
    try:
        model_bytes = await model.read()
        model_buffer = io.BytesIO(model_bytes)
        res_hash = get_hash(model_buffer)
        model_buffer.seek(0)
        minio.client.put_object(
            MINIO_MODELS_RES_BUCKET,
            model_id,
            model_buffer,
            len(model_bytes),
            metadata={
                "Content-Type": model.content_type,
            },
        )
        db_mod = MLModel.get_by_id(model_id)
        blockchain_svc.verify_contract(db_mod.contract_address, res_hash)
        db_mod.res_filename = model.filename
        db_mod.res_content_type = model.content_type
        db_mod.trained = True
        db_mod.save()
        return {"Trained model stored successfully."}
    except Exception as e:
        print(e)
        return {"Error verifying result of the model training job."}


@app.get("/model/{model_id}/metadata/")
async def get_model_md(model_id: str) -> ModelContract:
    """Fetch metadata for a registered model."""
    try:
        return get_model_metadata(model_id)
    except Exception as e:
        print(e)
        return f"Model {model_id} not found."
