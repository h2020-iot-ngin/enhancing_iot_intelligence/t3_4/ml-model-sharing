# Integration Test Example
This document covers an end-to-end use case scenario that involves all of the services of the platform.

1. Register a dataset, using the Model Sharing service REST API
2. Register a new ML model involving the dataset registered
3. Once the model has been trained, fetch the resulting model
4. Request the model to be translated, using the Model Translation service
