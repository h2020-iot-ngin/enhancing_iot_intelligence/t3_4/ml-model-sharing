# Model Translation service
This service offers the ability for the platform to transpile registered machine learning models into the Open Neural Network Exchange (ONNX) format upon training, offering a compatibility layer for all stored models in the platform.

