## Installation
The original guide is available [here](https://argoproj.github.io/argo-workflows/quick-start/).

0. Make sure `minikube` is installed in the local environment.
1. Start the local cluster with `minikube start`
2. `kubectl create namespace argo`
3. `minikube kubectl -- apply -n argo -f https://github.com/argoproj/argo-workflows/releases/download/v3.4.9/install.yaml`
4. Apply the following patch to the server `minikube kubectl -- patch deployment argo-server --namespace argo --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/args", "value": ["server","--auth-mode=server"]}]'`
5. Get the `ClusterIP` of the service with `minikube kubectl -- get services -n argo`
6. Start a tunnel to locally expose the service with `minikube tunnel`
7. Test that the IP from the step 5 is reachable locally, i.e., `GET https://<ClusterIP>:2746`
## Artifact Repository
0. You will need to run a minIO instance in the cluster. We recommend installation using Helm, with the following chart:
```bash
helm repo add minio https://helm.min.io/
helm repo update
helm install argo-artifacts minio/minio --set service.type=LoadBalancer --set fullnameOverride=argo-artifacts
```
1. Get the access key ID and secret:
```bash
kubectl get secret argo-artifacts --namespace default -o jsonpath="{.data.accesskey}" | base64 --decode
kubectl get secret argo-artifacts --namespace default -o jsonpath="{.data.secretkey}" | base64 --decode
```
2. Create a bucket for storing the artifacts.
3. Create the `artefact-repositories` `ConfigMap` ([guide](https://argoproj.github.io/argo-workflows/artifact-repository-ref/)).
4. Make sure the required container registry credentials are present in the cluster (`regcred`).
