# Generating Requirements

`requirements.txt` files to be used for building the Docker images of the services can be generated with Poetry, using the following command:

```bash
poetry export -f requirements.txt > requirements.txt
```
