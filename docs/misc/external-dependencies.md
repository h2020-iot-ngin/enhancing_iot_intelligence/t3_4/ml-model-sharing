# External Dependencies
In order to run the project end-to-end, the following external dependencies are required by the services:
- PostgreSQL instance and database
- ConsenSys Quorum instance
- MinIO instance
- Argo Workflows instance


## PostgreSQL instance
The [PostgreSQL](https://www.postgresql.org) instance is required in order to host the relational database that the Model Sharing service depends on for storing additional information about the models registered in the platform.

The [Peewee](http://docs.peewee-orm.com/en/latest/) ORM is used in order to manage the database connection, schema and models — database initialization is also 
handled by Peewee, and no manual initialization steps are needed, except for creating the target database in the PostgreSQL instance.


## ConsenSys Quorum instance
The [ConsenSys Quorum](https://consensys.net/quorum/) blockchain instance is required by the Blockchain service, in order to store the contracts for the datasets and models registered in the platform.


## MinIO instance
The MinIO instance requires a custom bucket structure to be set before the Model Sharing service connects to it for the first time. If the required buckets are
not in place, the service will fail to start — displaying a message with guidance on which buckets were missing.

There must exist three buckets in the instance, with full access (read, write, delete) granted to the user provided for the service:
- datasets bucket
- trained models bucket (resulting model after training)


## Argo Workflows instance
The [Argo Workflows](https://argoproj.github.io/argo-workflows/) instance is required by the Model Training service, in order to schedule the model training jobs.

Argo Workflows allows for execution of sequential jobs in Kubernetes Clusters. In our use case, each model registered on the platform triggers a model training job, which executes a user-defined container image, providing it with the associated model dataset, and registers the resulting model in the platform.

The Argo Workflows instance will exclusively be accessed through its REST API, by the Model Training service.
