# Copyright 2023, Atos Spain S.A.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from typing import Literal, Union

from .core.config import (
    CONTRACT_TYPES,
    W3_ADDRESS,
    W3_KEY,
    W3_PROVIDER,
)
from .lib.solidity import (
    Contract,
    ModelContractParams,
    ModelContractParamsUnverified,
    DatasetContractParams,
    W3Client,
)
from fastapi import FastAPI
from mlaas_sdk.models import (
    DatasetContract,
    ModelContract,
    ModelContractUnverified,
)
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware


app = FastAPI(
    middleware=[
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_methods=["*"],
            allow_headers=["*"],
        )
    ]
)

# Initialize Solidity client
w3 = W3Client(W3_PROVIDER, W3_ADDRESS, W3_KEY)


@app.get("/ping")
async def ping():
    """Liveness probe endpoint."""
    return "pong"


@app.post("/contract/{contract_type}")
async def deploy_contract(
    contract_type: Literal["model", "model_unverified", "dataset"],
    params: Union[ModelContract, ModelContractUnverified, DatasetContract],
) -> dict:
    try:
        match contract_type:
            case "model":
                params = ModelContractParams(
                    params.model_params.dict(), params.data_params.dict()
                )
            case "model_unverified":
                params = ModelContractParamsUnverified(params.dict())
            case "dataset":
                params = DatasetContractParams(params.dict())
        contract = Contract(CONTRACT_TYPES[contract_type])
        contract_addr = w3.deploy_contract(contract, params.get())
        return {"contract_address": contract_addr}
    except Exception as e:
        print(e)
        return "Error deploying contract."


@app.post("/contract/{address}/verify")
async def verify_contract(address: str, res_hash: str):
    try:
        contract = Contract(CONTRACT_TYPES["model"])
        w3.call_contract_function(address, contract, "verify", [res_hash])
        return "Model verified."
    except Exception as e:
        print(e)
        return f"Error verifying contract {address} (already verified?).)"


@app.get("/contract/{contract_type}/{address}")
async def read_contract(
    address: str,
    contract_type: Literal["model", "model_unverified", "dataset"],
) -> Union[ModelContract, ModelContractUnverified, DatasetContract]:
    """Reads a contract and returns its metadata."""
    try:
        meta = w3.read_contract(
            Contract(CONTRACT_TYPES[contract_type]), address
        )
        match contract_type:
            case "model":
                model_params = ModelContract.ModelParams.parse_obj(
                    {
                        "developer_id": meta[1][0],
                        "organization_id": meta[1][1],
                        "train_image": meta[1][2],
                        "res_hash": meta[1][3]
                        if meta[1].__len__() > 3
                        else None,
                    }
                )
                data_params = ModelContract.ModelDataParams.parse_obj(
                    {"dataset_id": meta[0]}
                )
                return ModelContract(
                    model_params=model_params,
                    data_params=data_params,
                )
            case "model_unverified":
                return ModelContractUnverified.parse_obj(
                    {
                        "developer_id": meta[0][0],
                        "organization_id": meta[0][1],
                        "hash": meta[0][2],
                    }
                )
            case "dataset":
                return DatasetContract.parse_obj(
                    {
                        "organization_id": meta[0][0],
                        "samples_dimension": meta[0][1],
                        "size_bytes": meta[0][2],
                        "hash": meta[0][3],
                    }
                )
            case _:
                return "Invalid contract type."
    except Exception as e:
        print(e)
        return f"Error retrieving contract {address}"
