# Local Environment Setup
There are two main ways of running the platform in a local development environment:
1. Running each service's project in the host OS
2. Running each service as a container

All of the services provide container images, which can be run using [Podman](https://podman.io).

The environment variables required by each service are documented in their respective `.env.sample` files.

In this document we will detail how to setup a development environment for running the platform locally.

More information regarding external dependencies is provided [here](./external-dependencies.md).


## External dependencies
### PostgreSQL instance
You can run a PostgreSQL instance locally with Podman using the following command:

```bash
podman run --name postgres-model-sharing -p 5432:5432 -e POSTGRES_PASSWORD=<password> -e POSTGRES_USER=<user> -e POSTGRES_PASSWORD=<pass> -e POSTGRES_DB=<db_name> -d postgres
```

### ConsenSys Quorum Instance
Please, refer to the [following official guide](https://consensys.net/quorum/products/guides/getting-started-with-consensys-quorum/) for running a Quorum instance locally, using a local Kubernetes cluster (e.g. [minikube](https://minikube.sigs.k8s.io/docs/)).

Alternatively, follow [this](./consensys-local.md) quick-start guide.


### MinIO Instance
You can run a MinIO instance locally with Podman using the following command:

```bash
podman run -d -p 9000:9000 -p 9001:9001   quay.io/minio/minio server /data --console-address ":9001" 
```

Once the instance is running, remember to setup the required initial buckets structure ([read more](../README.md#minio-instance)).


### Argo Workflows Instance
Please, refer to the [following official guide](https://argoproj.github.io/argo-workflows/quick-start/) for running an Argo Workflows instance locally, using a local Kubernetes cluster (e.g. [minikube](https://minikube.sigs.k8s.io/docs/)).


## Running Services — Non-Containerized Approach
As the project has been designed following a microservices-based approach, it is possible to launch the platform's components individually; however, please note that in most scenarios the platform's features are result of the collaboration with multiple services, and integration tests will usually require multiple services to be running concurrently.

There are two ways of running the microservices locally:
1. Set up a virtual environment and run the microservice in the host OS
2. Run the container image provided for the microservice

### Running Services in Host OS
For each of the services, it's recommended to setup a Python virtual environment in order to avoid conflicts in the project's dependencies. The dependencies required by each service are compiled in their respective requirements.txt, and can be installed using the `pip` CLI tool.

The recommended way to setup the virtual environment and install the dependencies of the services is by using Poetry. For this, execute the following command in the service's root directory:

```bash
poetry install
```

By default, by running this command, Poetry will create a virtual environment, installing on it all of the required dependencies. Additionally, upon completion of the command, Poetry will activate the virtual environment. 

When running the services in the host OS (not containerized), they will by default look for a `.env` file in their root directory. This `.env` file must comform to the structure defined in the service's `.env.sample` file.

You can launch any of the services using the following command:

```bash
python -m uvicorn services.<service>.app.main:app --port <port> --reload --log-level trace
```

### Running Services in Containers
You can run any of the services containers locally with Podman using the following command:

```bash
podman run -d -p <port>:<port> -env-file .env registry.gitlab.com/h2020-iot-ngin/enhancing_iot_intelligence/t3_4/ml-model-sharing/<service> <service_name_podman>  
```

Note that we are providing the service with the required environment variables by pointing to a `.env` file; this `.env` file must comform to the structure defined in the service's `.env.sample` file.
