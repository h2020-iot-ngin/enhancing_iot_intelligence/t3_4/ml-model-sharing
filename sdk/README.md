# IoT-NGIN — MLaaS Polyglot Model Sharing Framework Python SDK
Python SDK for interfacing with the IoT-NGIN MLaaS Polyglot Model Sharing Framework's services.

More information available at the [project's repository](https://gitlab.com/h2020-iot-ngin/enhancing_iot_intelligence/t3_4/ml-model-sharing).