# Copyright 2023, Atos Spain S.A.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os

import numpy as np
from ast import literal_eval
import torch
import torch.nn as nn
from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader
from environs import Env
from tqdm.notebook import tqdm
from scipy.io import loadmat

env = Env()
env.read_env()

OUTPUT_PATH = env("OUTPUT_PATH")
DATA_FILE = env("DATA_FILE")
DEVICE = env("DEVICE")
OUTPUT_MODEL_FILE = os.path.join(env("OUTPUT_MODEL_FILE"), f"mnist.pth")
N_EPOCH = env("N_EPOCH")
N_CLASSES = env("N_CLASSES")
SEED = literal_eval(env("SEED"))
LEARNING_RATE = env("LEARNING_RATE")
BATCH_SIZE = env("BATCH_SIZE")


def to_numpy(tensor):
    return (
        tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()
    )


def read_mnist_data(data_file=DATA_FILE):
    mnist = loadmat(data_file)
    data = mnist["data"].T
    labels = mnist["label"][0].astype(np.float)

    return data, labels


class MNISTDataset(Dataset):
    def __init__(self, X, y):
        self.X = X
        self.y = y

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        image = self.X[idx].reshape((28, 28))
        label = self.y[idx]

        return image, label


class MNISTNetwork(nn.Module):
    def __init__(self):
        super(MNISTNetwork, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(
                in_channels=1, out_channels=16, kernel_size=3, stride=1, padding=0
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(
                in_channels=16, out_channels=32, kernel_size=3, stride=1, padding=0
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
        )

        self.classifier = nn.Sequential(
            nn.Flatten(),
            nn.Linear(in_features=800, out_features=1024, bias=True),
            nn.ReLU(),
            nn.Linear(in_features=1024, out_features=512, bias=True),
            nn.ReLU(),
            nn.Linear(in_features=512, out_features=10, bias=True),
            nn.Softmax(dim=1),
        )

    def forward(self, x):
        x = self.features(x)
        return self.classifier(x)


def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    train_loss = 0

    model.train()

    progress = tqdm(enumerate(dataloader), total=len(dataloader))
    for batch, (X, y) in progress:
        X = X.to(DEVICE).unsqueeze(1)
        y = y.to(DEVICE)

        # Compute prediction and loss
        pred = model(X.float())
        loss = loss_fn(pred, y.long())

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        train_loss += np.mean(loss.item())

    return train_loss / size


def validation_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)

    accuracy = 0
    valid_loss = 0

    model.eval()

    with torch.no_grad():
        for X, y in dataloader:
            X = X.float().to(DEVICE).unsqueeze(1)
            y = y.to(DEVICE)

            pred = model(X)

            valid_loss += loss_fn(pred, y.long()).item()
            accuracy += (pred.argmax(1) == y).type(torch.float).sum().item()

    valid_loss /= size
    accuracy /= size

    print(f"Accuracy: {(100*accuracy):>0.1f}%, Avg loss: {valid_loss:>8f} \n")
    return valid_loss, accuracy


def save_model(model, save_path=OUTPUT_MODEL_FILE):
    torch.save(model.state_dict(), save_path)


def main():
    mnist, labels = read_mnist_data()

    X_train, X_vaild, y_train, y_vaild = train_test_split(
        mnist, labels, test_size=0.3, random_state=42
    )

    train_set = MNISTDataset(X_train, y_train)
    valid_set = MNISTDataset(X_vaild, y_vaild)

    train_loader = DataLoader(train_set, batch_size=64, shuffle=True)
    valid_loader = DataLoader(valid_set, batch_size=64, shuffle=True)

    model = MNISTNetwork().to(DEVICE)
    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=LEARNING_RATE)

    for t in range(N_EPOCH):
        train_loss = train_loop(train_loader, model, loss_fn, optimizer)
        valid_loss, accuracy = validation_loop(valid_loader, model, loss_fn)

    save_model(model)
