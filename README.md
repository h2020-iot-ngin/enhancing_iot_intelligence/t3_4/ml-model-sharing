# IoT-NGIN — Machine Learning Models Sharing Platform
Please, refer to the [/docs](./docs) directory for further information about the platform's architecture and capabilities.

The Machine Learning Models Sharing Platfom offers a secure environment to manage datasets and machine learning models' lifecycle, offering a user-friendly interface that abstracts end users from the complexities involved ([Zero-Knowledge Verification](./docs/misc/zero-knowledge-verification.md)).

The platform offers a transparent data integrity mechanism, which guarantees the inmutability and complete traceability of all artifacts stored; this is achieved by using a Blockchain-based approach for storing relevant metadata for all of its assets.


## Core Functionalities
1. End users can register and access datasets in the platform
2. The platform guarantees the integrity of registered datasets by storing relevant dataset metadata in the blockchain
3. Registered datasets can be used for training machine learning models registered in the platform
4. End users can register new machine learning models in the platform
5. Registered machine learning models will be trained with user-defined datasets stored in the platform
6. Model training results are guaranteed to be replicable, due to enforced isolation in the model training process, as well as storing of relevant metadata of the model training in the blockchain
7. Model training results' integrity is guaranteed by the stored metadata in the blockchain 
8. Registered models can be transpiled into Open Neural Network Exchange format (model translation) upon training, offering a compatibility layer for all stored models in the platform


## Platform Architecture
The platform has been developed following a microservices-based approach, with distinct, single purpose services. This decision was taken in corcondance to other key architectural aspects, mainly offering a Cloud Native solution, with a strong focus on DevOps & Continuous Integration / Development and containerization. 


## Platform Components
The platform consists of the following components:
- [Model Sharing service](#model-sharing-service)
- [Blockchain service](#blockchain-service)
- [Model Training service](#model-training-service)
- [Model Translation service](#model-translation-service)


### Model Sharing Service
This service provides an interface for registering and accessing ML models and datasets.

More information regarding the Model Sharing service is provided [here](./docs/services/model-sharing.md).


### Blockchain Service
This service provides an interface for deploying and interacting with smart contracts in a EVM blockchain (e.g. [ConsenSys Quorum](https://consensys.net/quorum/qbs/)).

*Note: the REST API exposed by this service is not intended for the end users of the platform, but only to be accessed directly by the rest of the services of the system.*

More information regarding the Blockchain service is provided [here](./docs/services/blockchain.md).


### Model Training Service
This service is in charge of scheduling model training jobs to be executed in the training cluster.

More information regarding the Model Training service is provided [here](./docs/services/model-training.md).


### Model Translation Service
This service is in charge of scheduling model training jobs to be executed in the training cluster.

More information regarding the Model Translation service is provided [here](./docs/services/model-translation.md).


## External Dependencies
In order to run the project end-to-end, the following external dependencies are required by the services:
- [PostgreSQL instance and database](./docs/misc/external-dependencies.md#postgresql-instance)
- [ConsenSys Quorum instance](./docs/misc/external-dependencies.md#consensys-quorum-instance)
- [MinIO instance](./docs/misc/external-dependencies.md#minio-instance)
- [Argo Workflows instance](./docs/misc/external-dependencies.md#argo-workflows-instance)

More information regarding external dependencies is provided [here](./docs/misc/external-dependencies.md).


## Local Development Environment
More information on how to provision a local development environment is provided [here](./docs/misc/local-env.md).


## Deployment on Kubernetes Cluster
More information on how to deploy the platform on a Kubernetes Cluster is provided [here](./docs/misc/deployment-kubernetes.md).
