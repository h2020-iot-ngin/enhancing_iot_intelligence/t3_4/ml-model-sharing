# Launching a Local ConsenSys Quorum Instance
## Pre-requisites
The host OS must have [Node.js](https://github.com/nvm-sh/nvm) and the [Docker runtime](https://www.docker.com) installed, in order to run ConsenSys' provisioning scripts.

If the host OS is Linux, we recommend installing Node.js using [Node Version Manager](https://github.com/nvm-sh/nvm).


## Installation
The original guide is available [here](https://web.archive.org/web/20220322045037/https://consensys.net/quorum/products/guides/getting-started-with-consensys-quorum/).
0. If no Node.js version is installed yet, run: `nvm install --lts`. 
1. Fetch and run ConsenSys' CLI tool: `npx quorum-dev-quickstart`
2. Choose option `1` (Hyperledger Besu)
3. Choose option `n` (no private transactions support)
4. Choose option `1` (logging with Loki)
5. Choose option `N` (no monitoring with Blockscout)
6. Press `ENTER` (create in directory `./quorum-test-network`)
6. Enter the directory `cd ./quorum-test-network`
7. Launch the instance with `sh ./run.sh`
8. Check the network is running successfully by visiting the [Web block explorer](http://localhost:25000/explorer/nodes)


Below you can find information about the pre-loaded accounts in the event you want to import them:

* Test Account 1 (address `0xfe3b557e8fb62b89f4916b721be55ceb828dbd73`)
    * Private key: `0x8f2a55949038a9610f50fb23b5883af3b4ecb3c3bb792cbcefbd1542c692be63`
    * Initial balance: `200 Eth (200000000000000000000 Wei)`
* Test Account 2 (address `0x627306090abaB3A6e1400e9345bC60c78a8BEf57`)
    * Private key: `0xc87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3`
    * Initial balance: `90000 Eth (90000000000000000000000 Wei) `
* Test Account 3 (address `0xf17f52151EbEF6C7334FAD080c5704D77216b732`)
    * Private key: `0xae6ae8e5ccbfb04590405997ee2d52d2b330726137b875053c36d94e974d162f`
    * Initial balance: `90000 Eth (90000000000000000000000 Wei)`