# Copyright 2023, Atos Spain S.A.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os
import re
import shutil
import sys

from urllib.parse import unquote
from mlaas_sdk.clients import SharingClient
from mlaas_sdk.exceptions import sharing as SharingExceptions

from .core.config import (
    DOWNLOAD_DIR,
    DATASET_ID,
    OUTPUT_DIR,
    SHARING_ENDPOINT,
)

sharing_svc = SharingClient(SHARING_ENDPOINT)


def download_dataset(dataset_id, download_path=DOWNLOAD_DIR, overwrite=False):
    if not os.path.exists(download_path):
        os.makedirs(download_path, exist_ok=True, mode=0o755)

    with sharing_svc.get_dataset(dataset_id) as r:
        filename = unquote(
            re.findall("(?<=utf-8'')(.*)$", r.headers["Content-Disposition"])[
                0
            ]
        )

        dataset_path = os.path.join(download_path, filename)

        if os.path.exists(dataset_path):
            if overwrite:
                os.remove(dataset_path)
            else:
                raise FileExistsError(
                    f"Dataset {dataset_id} already exists in {dataset_path}."
                )

        print(f"Downloading dataset to {dataset_path}")
        with open(
            dataset_path,
            "wb",
        ) as f:
            shutil.copyfileobj(r.raw, f)

    return dataset_path


if __name__ == "__main__":
    try:
        d = download_dataset(DATASET_ID, OUTPUT_DIR, overwrite=True)
        if DOWNLOAD_DIR != OUTPUT_DIR:
            shutil.copy(d, OUTPUT_DIR)
            os.remove(d)
    except shutil.SameFileError:
        sys.exit(1)
    except SharingExceptions.DatasetNotFoundException:
        print(f"Dataset {DATASET_ID} not found.")
