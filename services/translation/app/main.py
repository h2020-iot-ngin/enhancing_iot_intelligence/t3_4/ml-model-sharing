# Copyright 2023, Atos Spain S.A.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import re
import os

from .core.config import (
    SHARING_ENDPOINT,
    DOWNLOAD_DIR,
)

from fastapi import FastAPI
from mlaas_sdk.clients import SharingClient
from mlaas_sdk.models import ModelContract
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
import shutil

app = FastAPI(
    middleware=[
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_methods=["*"],
            allow_headers=["*"],
        )
    ]
)

sharing_svc = SharingClient(SHARING_ENDPOINT)


@app.get("/ping")
async def ping():
    """Liveness probe endpoint."""
    return "pong"


@app.post("/translate/{model_id}")
async def add_model(model_id):
    try:
        with sharing_svc.get_model(model_id) as r:
            r = sharing_svc.get_model(model_id)
            down_dir = os.path.join(DOWNLOAD_DIR, model_id)
            if not os.path.exists(down_dir):
                os.makedirs(down_dir)
            else:
                shutil.rmtree(down_dir)
                os.makedirs(down_dir)
            file = os.path.join(
                down_dir,
                re.findall("filename=(.+)", r.headers["Content-Disposition"])[
                    0
                ].replace('"', ""),
            )
            with open(file, "wb") as f:
                shutil.copyfileobj(r.raw, f)
                if "tar" in file:
                    os.system(f"tar -xf {file} -C {down_dir}")
            out_path = os.path.join(down_dir, f"{model_id}.onnx")
            os.system(
                f"python -m tf2onnx.convert --saved-model {down_dir} --output {out_path}"
            )
            r = sharing_svc.add_model(
                out_path, sharing_svc.get_model_metadata(model_id)
            )
            return r
    except Exception as e:
        print(e)
        return {"Error translating model."}
